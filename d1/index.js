fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data));

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    // Prevents the page from reloading
    // prevents default behavior of event
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data)
        alert('Successfully added!');

        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null;
    });



    // // Adds an element in the end of an array
    // posts.push({
    //     // "id" - used for the unique ID of each posts
    //     id: count,
    //     // "title" and "values - will come from the "form-add-post"
    //     title: document.querySelector("#txt-title").value,
    //     body: document.querySelector("#txt-body").value
    // });

    // // count will increment everytime a new post is added
    // count++

    // console.log(posts);
    // alert("Post Successfully Added!");

    // showPosts();

});


// RETRIEVE POSTS
const showPosts = (posts) => {
    // "postEntries" - variable that will contain all the posts
    let postEntries = "";

    posts.forEach((post) => {
        // "+=" - adds tha value of the right operand to a variable and assigns the result to the variable
        // "div" - division on where to place the value of title and posts
        postEntries += `
            <div id ="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });
    // To check what is stored in the properties of variables
    // console.log(postEntries);
    document.querySelector("#div-post-entries").innerHTML = postEntries;

};


// EDIT POST
const editPost = (id) => {
    
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

    document.querySelector('#btn-submit-update').removeAttribute('disabled');
};


// UPDATE POST
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    
    fetch('https://jsonplaceholder.typicode.com/posts/', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: {'Content-type': 'application/json; charset=UTF-8'} 
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data)
        alert('Successfully updated!');

        document.querySelector('#txt-edit-id').value = null;
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;

        document.querySelector('#btn-submit-update').setAttribute('disabled', true)

    });
});


// DELETE POST
const deletePost = (id) => {
    // // Loops through all elements of the array to find the post ID that matches the post selected
    // posts = posts.filter((post) => {
    //     if (post.id.toString() !== id) {
    //         return post;
    //     }
    // });
    // // The Element.remove() method removes the element from the DOM.
fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: 'DELETE'})
    document.querySelector(`#post-${id}`).remove();
};



